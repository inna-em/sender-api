import pytest
from rest_framework.test import APIClient
from sender_api.sender.models import Client


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def client_data():
    return {
        "phone_number": "79991234567",
        "mobile_operator_code": "999",
        "tag": "Anytag",
    }


def test_create_client(api_client, client_data):
    response = api_client.post("/clients/", client_data, format="json")
    assert response.status_code == 201
    assert Client.objects.count() == 1
    assert Client.objects.get().mobile_operator_code == "999"
    assert Client.objects.get().timezone == "Europe/Moscow"


def test_that_tests_work():
    assert 1 == 1
