from django.contrib import admin
from django.urls import include, path
from django.urls import path
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

api_info = openapi.Info(
        title="Sender API",
        default_version='v1',
        description="Service for managing sendings",
        terms_of_service="https://www.google.com/policies/terms/",
        license=openapi.License(name="BSD License"),
    )

schema_view = get_schema_view(
    api_info,
    public=True,
    permission_classes=[permissions.AllowAny,],
    url='https://localhost:8000/sender/schema.yaml'
)

urlpatterns = [
    path('docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('admin/', admin.site.urls),
    path('sender/', include('sender.urls')),
]

