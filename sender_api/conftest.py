import os
import django
import pytest
from django.conf import settings
from django.db import connection

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sender_api.core.settings')
django.setup()


@pytest.fixture(scope='session')
def django_db_setup():
    settings.DATABASES['default'] = settings.DATABASES['test']
    connection.ensure_connection()
