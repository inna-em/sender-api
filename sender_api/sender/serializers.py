from rest_framework import serializers
from .models import Client, Sending
from .tasks import process_sending


class ClientSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    phone_number = serializers.IntegerField(min_value=70000000000, max_value=79999999999)
    mobile_operator_code = serializers.IntegerField(min_value=100, max_value=999)
    tag = serializers.CharField(max_length=255)
    timezone = serializers.CharField(max_length=50)

    def create(self, validated_data):
        return Client.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.phone_number = validated_data.get('phone_number', instance.phone_number)
        instance.mobile_operator_code = validated_data.get('mobile_operator_code', instance.mobile_operator_code)
        instance.tag = validated_data.get('tag', instance.tag)
        instance.timezone = validated_data.get('timezone', instance.timezone)
        instance.save()
        return instance


class SendingSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    start_dttm = serializers.DateTimeField()
    message_text = serializers.CharField(max_length=10000)
    filters = serializers.JSONField()
    end_dttm = serializers.DateTimeField()

    def create(self, validated_data):
        obj = Sending.objects.create(**validated_data)
        process_sending(obj.id, obj.start_dttm, obj.end_dttm)
        return obj

    def update(self, instance, validated_data):
        instance.start_dttm = validated_data.get('start_dttm', instance.start_dttm)
        instance.message_text = validated_data.get('message_text', instance.message_text)
        instance.filters = validated_data.get('filters', instance.filters)
        instance.end_dttm = validated_data.get('end_dttm', instance.end_dttm)
        instance.save()
        process_sending(instance.id, instance.start_dttm, instance.end_dttm)
        return instance
