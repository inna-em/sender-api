from django.urls import path
from .views import ClientListView, ClientView, SendingListView, SendingView, message_count, total_statistics

app_name = 'sender'

urlpatterns = [
    path('clients/', ClientListView.as_view(), name='client-create'),
    path('clients/<str:id>/', ClientView.as_view(), name='client-update'),
    path('clients/<str:id>/', ClientView.as_view(), name='client-delete'),
    path('sendings/', SendingListView.as_view(), name='sending-create'),
    path('sendings/<str:id>/', SendingView.as_view(), name='sending-update'),
    path('sendings/<str:id>/', SendingView.as_view(), name='sending-delete'),
    path('sendings/<str:sending_id>/statistics/', message_count, name='sending-statistics'),
    path('total_statistics/', total_statistics, name='total-statistics'),
]
