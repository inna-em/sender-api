from django.db import models
from django.utils import timezone
from django_enumfield import enum
from django.core.validators import MaxValueValidator, MinValueValidator


class Sending(models.Model):
    start_dttm = models.DateTimeField()
    message_text = models.TextField()
    filters = models.JSONField()
    end_dttm = models.DateTimeField()


class Client(models.Model):
    phone_number = models.BigIntegerField(validators=[MinValueValidator(70000000000), MaxValueValidator(79999999999)])
    mobile_operator_code = models.IntegerField(validators=[MinValueValidator(100), MaxValueValidator(999)])
    tag = models.CharField(max_length=255)
    timezone = models.CharField(max_length=50, default='Europe/Moscow')


class SendingStatus(enum.Enum):
    PENDING = 1
    SUCCESS = 2
    ERROR = 3
    STOPPED = 4


class Message(models.Model):
    created_dttm = models.DateTimeField(default=timezone.now)
    sending_status = enum.EnumField(SendingStatus, default=SendingStatus.PENDING)
    sending = models.ForeignKey(Sending, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
