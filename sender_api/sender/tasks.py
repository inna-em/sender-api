import logging
import requests
from celery import shared_task, group
from django.utils import timezone
from .models import Sending, Client, Message, SendingStatus
from core.celery_app import app
from django.conf import settings

logger = logging.getLogger('sender_logger')
logger.setLevel(logging.DEBUG)


@shared_task
def process_sending(sending_id, start_time, end_time):
    # Check if the end_dttm is in the past, and if so, do nothing
    if end_time <= timezone.now():
        logger.error("Could not schedule an outdated delivery with id " + str(sending_id))
        return

    # Calculate the start time of the task processing interval
    start_time = max(timezone.now(), start_time)

    # Schedule a task to process the sending at the start_time
    logger.info("Scheduling sending task for sending " + str(sending_id))
    process_sending_task.apply_async((sending_id,), eta=start_time)

    # Schedule a task to stop the sending at the end_time
    stop_sending_task.apply_async((sending_id,), eta=end_time)


def call_external_api(message_id, phone, text):
    headers = {
        'Authorization': f'Token {settings.API_TOKEN}',
        'Content-Type': 'application/json',
    }
    data = {
        'id': message_id,
        'phone': phone,
        'text': text,
    }
    response = requests.post('https://api.example.com/send_message/', headers=headers, json=data)
    return response


@app.task(name="process_message_task")
def process_message_task(message_id, retry_count=0):
    try:
        message = Message.objects.get(id=message_id)
    except Message.DoesNotExist:
        logger.error(f"Message with id={message_id} does not exist")
        return

    if message.sending_status == SendingStatus.STOPPED:
        logger.info(f"Sending has been stopped. Skipping message {message.id}")
        return

    api_response = call_external_api(message.id, message.client.phone_number, message.sending.message_text)

    if api_response.status_code == 200:
        message.status = SendingStatus.SUCCESS
        message.save()
    elif retry_count < settings.MAX_RETRY_COUNT:
        retry_count += 1
        process_message_task.apply_async(args=(message_id, retry_count), countdown=settings.RETRY_DELAY)
    else:
        message.status = SendingStatus.ERROR
        message.save()

    return


@app.task(name="process_messages_group")
def process_messages_group(messages):
    tasks = []
    for message in messages:
        tasks.append(process_message_task.si(message.id))

    messages_group = group(tasks)
    messages_group()


@shared_task
def process_sending_task(sending_id: int):
    logger.info("Starting sending process with id " + str(sending_id))
    try:
        sending = Sending.objects.get(id=sending_id)
    except Sending.DoesNotExist:
        logger.error("Could not find a sending with id " + str(sending_id))
        return

    filters = sending.filters
    tag = filters.get("tag")
    mobile_operator_code = filters.get("mobile_operator_code")

    if tag is not None and mobile_operator_code is not None:
        clients = Client.objects.filter(tag=tag, mobile_operator_code=mobile_operator_code)
        if not clients:
            logger.error("No matching clients. Stopping sending process with id " + str(sending_id))
            return
        messages = []
        for client in clients:
            message = Message.objects.create(sending=sending, client=client)
            messages.append(message)
        process_messages_group(messages)
    else:
        logger.error("Couldn't find proper filters. Stopping sending process with id " + str(sending.id))
    return


@shared_task
def stop_sending_task(sending_id: int):
    try:
        sending = Sending.objects.get(id=sending_id)
    except Sending.DoesNotExist:
        logger.error("Could not find a sending with id " + str(sending_id))
        return

    messages = Message.objects.filter(sending=sending)
    for message in messages:
        if message.status != SendingStatus.SUCCESS:
            message.status = SendingStatus.STOPPED
            message.save()

    logger.info("Stopping sending process with id " + str(sending_id))
    return
