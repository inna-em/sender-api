from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from django.http import JsonResponse
from django.db import models
from .models import Client, Sending, Message
from .serializers import ClientSerializer, SendingSerializer


class ClientListView(ListCreateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ClientView(RetrieveUpdateDestroyAPIView):
    queryset = Client.objects.all()
    lookup_field = 'id'
    serializer_class = ClientSerializer


class SendingListView(ListCreateAPIView):
    queryset = Sending.objects.all()
    serializer_class = SendingSerializer


class SendingView(RetrieveUpdateDestroyAPIView):
    queryset = Sending.objects.all()
    lookup_field = 'id'
    serializer_class = SendingSerializer


def message_count(request, sending_id):
    try:
        messages = Message.objects.filter(sending=sending_id).values('sending_status').annotate(count=models.Count('sending_status'))
        response_data = {'message_count': list(messages)}
        return JsonResponse(response_data, status=200)
    except Message.DoesNotExist:
        response_data = {'message_count': []}
        return JsonResponse(response_data, status=404)


def total_statistics(request):
    sendings = Sending.objects.all()
    sendings_list = []
    for sending in sendings:
        messages = Message.objects.filter(sending=sending.id).values('sending_status').annotate(count=models.Count('sending_status'))
        sendings_list.append({
            'sending_id': sending.id,
            'message_count': list(messages)
        })
    return JsonResponse({'sendings': sendings_list})
