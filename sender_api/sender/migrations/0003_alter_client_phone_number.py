# Generated by Django 4.2.1 on 2023-05-15 04:52

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sender', '0002_alter_client_mobile_operator_code_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='phone_number',
            field=models.BigIntegerField(validators=[django.core.validators.MinValueValidator(70000000000), django.core.validators.MaxValueValidator(79999999999)]),
        ),
    ]
